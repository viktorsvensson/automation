variable "public_key_path" {
  description = "The path to our public key"
  type        = string
  default     = "/home/vagrant/.ssh/id_rsa.pub"
}
