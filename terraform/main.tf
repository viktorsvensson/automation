terraform {

  cloud {
    organization = "gbg21-demo-ec"
    workspaces {
      name = "test"
    }
  }


  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.36.1"
    }
  }
}

provider "aws" {
  region = "eu-north-1"
}

resource "aws_key_pair" "rsa_key" {
  key_name   = "rsa_key"
  public_key = file(var.public_key_path)
}

resource "aws_instance" "web" {
  ami                    = "ami-051a4a0c60c88f085"
  instance_type          = "t3.micro"
  key_name               = aws_key_pair.rsa_key.id
  vpc_security_group_ids = [aws_security_group.ec2.id]
}

output "public_ip" {
  value = aws_instance.web.public_ip
}

resource "aws_security_group" "ec2" {

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

