resource "aws_instance" "web" {
  ami           = "ami-051a4a0c60c88f085"
  instance_type = "t3.micro"

  tags = {
    Name = var.instance_name
  }
}
